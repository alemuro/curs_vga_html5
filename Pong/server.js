var io = require('socket.io').listen(27015);
io.set('log level', 1);

var players = {};
var nump = 0;
var ball = new Ball();

io.sockets.on('connection', function (socket) {

	console.log('A socket with ID '+socket.id+'has connected to the server!');
	++nump;
	var spec = false;
	if (nump > 2) spec = true;
	players[socket.id] = new Player (socket);

	socket.emit('logged', {w: WIDTH, h: HEIGHT});

	if (nump == 2) startGame();

	var sockets = [];
	for (var playerID in players) sockets.push(players[playerID].socket);
	
	sendGameState(sockets);

	socket.on('changeDirection', function (newDir) { //x,y,vx,vy
		var p = players[socket.id];
		p.x = newDir.x;
		p.y = newDir.y;
	});
});

var WIDTH = 950;
var HEIGHT = 600;

function startGame() {
	for (playerID in players) {
		players[playerID].socket.emit("startGame");
	}
	ball.vx = ball.vy = -1;
}

function Player(socket) {
	this.socket = socket;
	this.i = (HEIGHT-this.mida)/2;
	this.score = 0;
}
Player.prototype.mida = 75;
Player.prototype.speed = 10;

Player.prototype.disconnect = function () {
	delete players[this.socket.id];
	needsGameStateUpdate = true;
}

function Ball() {
	this.x = WIDTH/2;
	this.y = HEIGHT/2;
	this.vx = this.vy = 0;
	this.speed = 5;
}

Ball.prototype.logic = function() {
	if (this.y > HEIGHT-this.radius || this.y < 0) this.vy *= -1; 
	// Comprovació del Player 1
	if (this.x < 10 && this.x+this.radius > 0 && (this.y+this.radius) > players[0].i && this.y < players[0].i+players[0].mida) this.vx *= -1;
	// Comprovació del Player 2
	if (this.x+this.radius > (WIDTH-10) && this.x < WIDTH && (this.y+this.radius) > players[1].i && this.y < players[1].i+players[1].mida) this.vx *= -1;
	// Increment de la velocitat
	this.x += this.vx*this.speed*delta/16;
	this.y += this.vy*this.speed*delta/16;
}

Ball.prototype.generatePacket  = function() {
	var packet = {};
	packet.x = this.x;
	packet.y = this.y;
	packet.vx = this.vx;
	packet.vy = this.vy;
	packet.speed = this.speed;
	return packet;
}		

Player.prototype.generatePacket = function () {
	var packet = {};
	packet.i = this.i;
	packet.score = this.score;
	return packet;
}

function sendGameState(sockets) {
	var playersPacket = {};
	for (var playerID in players) {
		playersPacket[playerID] = players[playerID].generatePacket();
	}
	for (var i = 0; i < sockets.length; ++i) {
		sockets[i].emit('gameState', {players:playersPacket, ball: ball.generatePacket()});
	}
}


var oldDate = +new Date();
var delta = 0;

function mainLoop () {

	var currentDate = +new Date();
	delta = currentDate - oldDate;
	oldDate = currentDate;

	needsGameStateUpdate = false;

	for (var playerID in players) {
		if (players[playerID].socket.disconnected) players[playerID].disconnect();
		else players[playerID].logic();
	}
	
	if (needsGameStateUpdate) {
		var sockets = [];
		for (var playerID in players) {
			sockets.push(players[playerID].socket);
		}
		sendGameState(sockets);
	}
}
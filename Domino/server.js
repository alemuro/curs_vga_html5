var io = require('socket.io').listen(8000);
io.set('log level', 1);

var partides = [];

io.sockets.on('connection', function (socket) {

	console.log('A socket with ID '+socket.id+'has connected to the server!');
	
	var partidaTrobada = -1;
	for (var i = 0; i < partides.length; ++i) {
		if (partides[i].players[1] == null) {
			partides[i].players.push(new Player(socket));
			partidaTrobada = i;
		}
	}

	if (partidaTrobada == -1) {
		var p = new Player(socket);
		partides.push(new Partida(p));
		partidaTrobada = partides.length -1;
	}

	console.log('Socket '+socket.id+' associated with game ID '+partidaTrobada);

	socket.emit('logged', {socket:socket.id, idpartida: partidaTrobada});

	/*for (var playerID in players) sockets.push(players[playerID].socket);
	sendGameState(sockets);*/

	socket.on('gameStart', function (partidaID) {
		var p = partides[partidaID];
		var start = 0;
		for (var i = 0; i < aux.players.length; ++i) {
			if (p.players[i].socket.id == socket.id) {
				p.players[i].socket.id = true;
				console.log(p.players[i].socket.id + " want's to start!");
			}
			if (p.players[i].start) ++start;
		}
		if (start == 2) startGame(partidaID);
	});

});

function Partida(player) {
	this.players = [player,null];
}

function Player(socket) {
	this.socket = socket;
	this.start = false;
}

function startGame(partidaID) {
	console.log(partidaID + ' has started');
}